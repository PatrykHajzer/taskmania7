package com.patryk.taskmania7.service;

import com.patryk.taskmania7.model.Task;

import java.util.List;
import java.util.Optional;

public interface TaskService {
    void saveTask(String taskName, String description);
    List<Task> loadAllUserTasks();
    Optional<Task> loadTask(Long Id);
}
