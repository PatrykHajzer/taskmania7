package com.patryk.taskmania7.service;

import com.patryk.taskmania7.model.Role;
import com.patryk.taskmania7.model.User;
import com.patryk.taskmania7.repository.RoleRepository;
import com.patryk.taskmania7.repository.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class UserDetailsImpl extends User implements UserDetails {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    public UserDetailsImpl(UserRepository userRepository, RoleRepository roleRepository){
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public UserDetailsImpl(User user) {
        super(user);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles()
                .stream()
                .map(role-> new SimpleGrantedAuthority("ROLE_"+role.getRole()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void register(User user){
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByRole("USER"));
        user.setRoles(roles);
        userRepository.save(user);
    }

//    public boolean login(String username, String password){
//        User userCheck = userRepository.findByUsername(username);
//        if (userCheck instanceof User){
//            if (userCheck.getPassword().equals(password)){
//                return true;
//            }
//            else return false;
//        }
//        else return false;
//
//    }
}
