package com.patryk.taskmania7.service;

import com.patryk.taskmania7.model.Task;
import com.patryk.taskmania7.model.User;
import com.patryk.taskmania7.repository.TaskRepository;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Optional;

public class TaskServiceImpl implements TaskService{

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }

    @Override
    public void saveTask(String taskName, String description){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user != null){
            Long userId = user.getId();
            Task task = new Task(userId, taskName, description);
            taskRepository.save(task);
        }
    }

    //future feature

    @Override
    public List<Task> loadAllUserTasks(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = user.getId();
            return taskRepository.findAllByUserId(userId);

    }

    @Override
    public Optional<Task> loadTask(Long Id){
        return taskRepository.findById(Id);
    }

}
