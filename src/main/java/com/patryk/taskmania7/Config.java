package com.patryk.taskmania7;


import com.patryk.taskmania7.repository.RoleRepository;
import com.patryk.taskmania7.repository.TaskRepository;
import com.patryk.taskmania7.repository.UserRepository;
import com.patryk.taskmania7.service.TaskServiceImpl;
import com.patryk.taskmania7.service.UserDetailsImpl;
import com.patryk.taskmania7.service.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
public class Config {



    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SimpleUrlAuthenticationFailureHandler simpleUrlAuthenticationFailureHandler() {
        return new SimpleUrlAuthenticationFailureHandler();
    }

    @Bean
    public TaskServiceImpl taskService(TaskRepository taskRepository){
        return new TaskServiceImpl(taskRepository);
    }
    @Bean
    public UserDetailsServiceImpl userDetailsService(UserRepository userRepository, RoleRepository roleRepository) {
        return new UserDetailsServiceImpl(userRepository, roleRepository);
    }
    @Bean
    public UserDetailsImpl userDetails(UserRepository userRepository, RoleRepository roleRepository){
        return new UserDetailsImpl(userRepository, roleRepository);
    }


//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**");
//            }
//        };
//    }


//    @Bean
//    public RestAuthenticationEntryPoint entryPoint(HttpServletRequest request, HttpServletResponse response, AuthenticationException auth){
//        return new RestAuthenticationEntryPoint(request, response, auth);
//    }
//    @Bean
//    public DataSource getDataSource() {
//        BasicDataSource dataSource = new BasicDataSource();
//        dataSource.setDriverClassName(env.getProperty("mysql.driver"));
//        dataSource.setUrl(env.getProperty("mysql.jdbcUrl"));
//        dataSource.setUsername(env.getProperty("mysql.username"));
//        dataSource.setPassword(env.getProperty("mysql.password"));
//        return dataSource;
//    }
}
