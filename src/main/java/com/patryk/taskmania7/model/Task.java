package com.patryk.taskmania7.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Entity
@Table(name = "task")
public class Task {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "task_id")
    private Long id;

    @NotEmpty(message = "*Please provide username")
    private Long userId;

    @NotEmpty(message = "*Please provide your name of task")
    @Column(nullable = false, unique = true)
    private String taskName;

    @Length(min = 10, message = "*Please put description about your task")
    @NotEmpty(message = "*Please provide description")
    private String description;



    public Task(Long userId, String taskName, String description){
        this.userId = userId;
        this.taskName = taskName;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
