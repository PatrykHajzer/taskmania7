package com.patryk.taskmania7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Taskmania7Application {

    public static void main(String[] args) {
        SpringApplication.run(Taskmania7Application.class, args);
    }

}

