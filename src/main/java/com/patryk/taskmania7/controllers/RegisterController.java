package com.patryk.taskmania7.controllers;

import com.patryk.taskmania7.model.User;
import com.patryk.taskmania7.service.UserDetailsServiceImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/")
public class RegisterController {

    private BCryptPasswordEncoder bCryptPasswordEncoder;
	private UserDetailsServiceImpl userDetails;



	public RegisterController(
	        BCryptPasswordEncoder bCryptPasswordEncoder,
            UserDetailsServiceImpl userDetails) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userDetails = userDetails;
	}


	@PostMapping(path = "/register")
	public String addNewUser(@RequestParam String username,
										   @RequestParam String password,
										   @RequestParam String email){
		User user = new User(username, bCryptPasswordEncoder.encode(password), email);
		userDetails.register(user);
		return "Saved";
	}

}
