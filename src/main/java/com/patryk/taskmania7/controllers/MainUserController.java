package com.patryk.taskmania7.controllers;

import com.patryk.taskmania7.model.Task;
import com.patryk.taskmania7.service.TaskServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/user")
public class MainUserController {

    private TaskServiceImpl taskService;

    public MainUserController(TaskServiceImpl taskService) {
        this.taskService = taskService;
    }

    @PostMapping(path = "/addTask")
    public String addTask(
            @RequestParam String taskName,
            @RequestParam String taskDescription){

        taskService.saveTask(taskName, taskDescription);
        return "task dodany";


    }
    @GetMapping(path = "/")
    public Iterable<Task> allTasks(){
        return taskService.loadAllUserTasks();
    }

    @GetMapping(path = "/{taskId}")
    public Optional<Task> loadTask(@PathVariable("taskId") Long taskId){
        return taskService.loadTask(taskId);

    }
}
